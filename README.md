# kddql

A personal GraphQL API.

https://kddql-api.now.sh/graphql

## Deployment

### [zeit now](https://zeit.co/now)

```bash
$ now && now alias
```
