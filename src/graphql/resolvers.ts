// tslint:disable object-literal-sort-keys
import { generate as generateId } from "shortid";

import { IGraphQLContext, ILabel, INote } from ".";

export const resolvers = {
  Query: {
    labels: async (
      _: any,
      __: any,
      { dataStore: { labels } }: IGraphQLContext
    ) => Object.values(labels),
    note: async (
      _: any,
      { id }: { id: string },
      { dataStore: { notes } }: IGraphQLContext
    ) => {
      return notes[id];
    },
    notes: async (
      _: any,
      __: any,
      { dataStore: { labels, notes } }: IGraphQLContext
    ) =>
      Object.values(notes)
        .map(note => {
          return {
            ...note,
            labels: note.labels.map(lid => labels[lid])
          };
        })
        .filter(n => n)
  },
  // tslint:disable-next-line object-literal-sort-keys
  Mutation: {
    createNote: async (
      _: any,
      {
        input: { labelIds, title }
      }: { input: { title: string; labelIds?: string[] } },
      { dataStore }: IGraphQLContext
    ) => {
      try {
        const note: INote = {
          id: generateId(),
          title,
          labels: []
        };

        if (labelIds) {
          note.labels = Object.values(dataStore.labels)
            .filter(label => labelIds.includes(label.id))
            .map(l => l.id);
        }

        dataStore.notes[note.id] = note;

        return {
          ...note,
          labels: note.labels.map(lid => dataStore.labels[lid])
        };
      } catch (err) {
        // TODO: Introduce apollo errors
        // tslint:disable-next-line no-console
        console.log("ERROR!", err);
        throw new Error(err);
      }
    },

    deleteNote: async (
      _: any,
      { id }: { id: string },
      { dataStore: { notes } }: IGraphQLContext
    ) => {
      if (notes[id]) {
        delete notes[id];

        return { id, success: true };
      }
      // TODO: Introduce apollo errors
      // tslint:disable-next-line no-console
      console.log(`ERROR DELETING NOTE ${id}`);
      return {
        id,
        success: false
      };
    },

    createLabel: async (
      _: any,
      {
        input: { color, title, noteId }
      }: { input: { title: string; color: string; noteId?: string } },
      { dataStore }: IGraphQLContext
    ) => {
      const label: ILabel = {
        color,
        id: generateId(),
        title
      };

      if (noteId) {
        // Find note
        const note = dataStore.notes[noteId];

        if (note) {
          note.labels.push(label.id);
        } else {
          throw new Error(`No note with id: ${noteId} exists`);
        }
      }

      dataStore.labels[label.id] = label;

      return label;
    },

    removeLabel: async (
      _: any,
      {
        input: { labelId, noteId }
      }: { input: { labelId: string; noteId: string } },
      { dataStore }: IGraphQLContext
    ) => {
      const note = dataStore.notes[noteId];

      note.labels = note.labels.filter(lid => lid !== labelId);

      return {
        ...note,
        labels: note.labels.map(lid => dataStore.labels[lid])
      };
    },

    addLabel: async (
      _: any,
      {
        input: { labelId, noteId }
      }: { input: { labelId: string; noteId: string } },
      { dataStore }: IGraphQLContext
    ) => {
      const note = dataStore.notes[noteId];

      note.labels.push(labelId);

      return {
        ...note,
        labels: note.labels.map(lid => dataStore.labels[lid])
      };
    }
  }
};
