import { ApolloServer, gql } from "apollo-server-express";
import * as fs from "fs";
import { resolve } from "path";
import { resolvers } from "./resolvers";

export interface ILabel {
  id: string;
  color: string;
  title: string;
}

export interface INote {
  id: string;
  title: string;
  labels: string[];
}

interface IDataStore {
  labels: {
    [id: string]: ILabel;
  };
  notes: {
    [id: string]: INote;
  };
}

const dataStore: IDataStore = {
  labels: {
    "1": {
      color: "magenta",
      id: "1",
      title: "graphql"
    },
    "2": {
      color: "royalblue",
      id: "2",
      title: "typescript"
    }
  },
  notes: {
    "1": {
      id: "1",
      labels: ["1"],
      title: "Launch GraphQL API"
    }
  }
};

export interface IGraphQLContext {
  dataStore: IDataStore;
}

export const typeDefs = gql(
  fs.readFileSync(resolve(__dirname, "schema.graphql"), "utf8")
);
export const createGraphQLServer = (): ApolloServer =>
  new ApolloServer({
    context: { dataStore },
    // TODO: DO NOT USE THIS FOR PRODUCTION!
    introspection: true,
    playground: true,
    // TODO: Properly type `resolvers`
    resolvers: resolvers as any,
    typeDefs
  });
