import { resolvers } from "../resolvers";

const mockStore = {
  labels: {},
  notes: {}
};

describe("resolvers", () => {
  describe("Query", () => {
    describe("users", () => {
      it("returns an empty collection when no Notes exist", async () => {
        const notes = await resolvers.Query.notes(null, null, {
          dataStore: mockStore
        });
        expect(notes).toEqual([]);
      });

      it("returns notes", async () => {
        const fakeNotes = [
          {
            title: "Sed consequatur et",
            id: "1",
            labels: []
          },
          {
            title: "Recusandae enim",
            id: "2",
            labels: []
          }
        ];
        const store = {
          notes: fakeNotes.reduce((accum: any, val) => {
            accum[val.id] = val;
            return accum;
          }, {}),
          labels: {}
        };
        const notes = await resolvers.Query.notes(null, null, {
          dataStore: store
        });

        expect(notes).toMatchInlineSnapshot(`
Array [
  Object {
    "id": "1",
    "labels": Array [],
    "title": "Sed consequatur et",
  },
  Object {
    "id": "2",
    "labels": Array [],
    "title": "Recusandae enim",
  },
]
`);
      });
    });
  });
});
