import { createGraphQLServer, typeDefs } from "..";
import { ApolloServer } from "../../../node_modules/apollo-server-express";

describe("GraphQL", () => {
  it("schema", () => {
    expect(typeDefs).toMatchSnapshot();
  });

  describe("createGraphQLServer", () => {
    it("returns an ApolloServer instance", () => {
      expect(createGraphQLServer()).toBeInstanceOf(ApolloServer);
    });
  });
});
