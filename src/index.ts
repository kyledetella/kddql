import * as bodyParser from "body-parser";
import * as express from "express";
import * as morgan from "morgan";
import { createGraphQLServer } from "./graphql";

import "./logging";

const app = express();
const { PORT = 3349 } = process.env;
app.use(bodyParser.json());
// TODO: :graphql logging should not be used in production!
app.use(morgan(":graphql"));
// app.use(morgan("combined"));

// Base API
app.get("/", (_, res) => {
  res.json({ status: "alive!" });
});

// Apollo
const graphQLServer = createGraphQLServer();
graphQLServer.applyMiddleware({ app, cors: true });

app.listen({ port: PORT }, () =>
  // tslint:disable-next-line no-console
  console.log(
    `🚀 Server ready at http://localhost:${PORT}${graphQLServer.graphqlPath}`
  )
);

export default app;
