import chalk from "chalk";
import * as morgan from "morgan";

// TODO: This is/will be too slow for production
// TODO: Move this to a module
morgan.token("graphql", req => {
  const { query, variables, operationName } = req.body;
  const title = chalk.bgMagenta.underline.bold("[GraphQL]");

  if (operationName === undefined) {
    return `${title} – Introspection query`;
  }

  return `${title}

${chalk.gray("Operation Name:")} ${operationName}

${chalk.gray("Query:")} ${query}

${chalk.gray("Variables:")} ${JSON.stringify(variables, null, 2)}

${chalk.bgMagenta.underline.bold("•••")}`;
});
